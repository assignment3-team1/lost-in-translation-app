import TranslationForm from "../Components/TranslationForm"
import withAuth from "../hoc/withAuth"
import NavBar from "../Components/NavBar"

const Translation = () =>{
    return(
        <div>
            <NavBar profile={false} />
            <TranslationForm/>
        </div>
    )
}

export default withAuth(Translation)