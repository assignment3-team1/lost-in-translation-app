import withAuth from "../hoc/withAuth"
import TranslationList from '../Components/TranslationList'
import LogOutButton from "../Components/LogOutButton"
import DeleteButton from "../Components/DeleteButton"
import NavBar from "../Components/NavBar"

const Profile = () =>{
    return(
        <>
        <NavBar profile={true} />
        <h2>Your latest translations: </h2>
            <TranslationList />
            <br/>
            <h3>Actions:</h3>
            <LogOutButton />
            <DeleteButton />
        </>
    )
}

export default withAuth(Profile)