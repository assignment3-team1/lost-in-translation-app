import { useForm } from "react-hook-form"
import { useState } from "react"
import { useUser } from "../Context/UserContext"
import { updateTranslations } from "../api/UserAPI"
import { storageSave} from "../utils/storage"
import spaceImage from "../assets/space.png"

const translationConfig = {
    required: true,
    maxLength: 40
}

const TranslationForm = () =>{
    const {register, handleSubmit, formState: { errors } } = useForm()
    const [translation, setTranslation] =  useState('');
    //get user ID
    const {user, setUser} = useUser()
   

    // const handleTranslation = (textTrans) => {
    //     //set text to translate staet if button is clicked
    //     translation = textTrans
    // }

    //create array of characters form the translate text
    const splitArray = translation.toLowerCase().split('');
    const translationArr = splitArray.map( (char, index) => {
        if(char === ' ') {
            //if char = space, return a image to show space in text
            return <img key={char + index} src={spaceImage} alt={'space'} />
        } else if ((char.charCodeAt(0) < 97 && char.charCodeAt(0) > 32) || char.charCodeAt(0) > 122) {
            //return nothing if character is somthing else than a smal letter (ASCII code)
            return
        }
        //Set image for view
        return <img key={char + index} src={`img/${char}.PNG`} alt={char} />
    })

    //When form is submitted
    const onSubmit =  async (data) =>{
        setTranslation('')
        const [error, transUser] = await updateTranslations(user, data.translations)
        
        //if no error - set User and set translation
        //Then store user
        if(error === null) {
            setUser(transUser);
            setTranslation(data.translations)
            storageSave('translationUser', transUser)
        }
        
    }

    return(
        <>
            <h1>Translation</h1>
            <form onSubmit={handleSubmit(onSubmit)}>
                <fieldset>
                    <label htmlFor="translate">Translate text: </label>
                <input type="text" placeholder="Hello" 
                {...register('translations', translationConfig)}/>
                
                <button type="submit"> Translate </button>
                {(errors.translations && errors.translations.type === 'required') && <span>Translation text is required</span>}
                {(errors.translations && errors.translations.type === 'maxLength') && <span>Max length 40</span>}
                </fieldset> 
            </form> 
            <div>{translationArr}</div>
        </>
        
    )
}

export default TranslationForm


