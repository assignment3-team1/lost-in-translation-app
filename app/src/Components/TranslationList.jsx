import { useUser } from "../Context/UserContext"

const TranslationList = () =>{

    const {user} = useUser();
    //get the translations from the user saved in the contextAPI
    let translations = user.translations;
    if (user.index) { //if user has a set index then only show translations with index higher than the user index
        translations = translations.slice(user.index);
    }
    if (translations.length > 10) { //only show the last 10 translations
        translations = translations.slice(-10);
    }

    // map each remaining translation to a list item, key is a combination of index and translation text. This will be unique
    //Since new translations are appended to the end of the list, will need to reverse this item list in the jsx return for the newest translation
    //to appear on top.
    const translationItems = translations.map( (translation, index) => {
        return <li key={translation+index}>{translation}</li>
    });

    

    return(
        <ul>
            {translationItems.reverse() }  
        </ul>
    )
}

export default TranslationList