import { NavLink } from 'react-router-dom'
import "../css/NavBar.css"

const NavBar = (props) => {
    return (
        <nav >
            <h2 >Translation App </h2>
                {props.profile ?<NavLink className="link" to="/translation" >Navigate: Translate</NavLink> :
                <NavLink className="link" to="/profile" >Navigate: Profile</NavLink> }
        </nav>
    )
}

export default NavBar;