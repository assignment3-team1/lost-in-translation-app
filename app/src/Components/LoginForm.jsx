import { useForm } from "react-hook-form"
import { useEffect } from "react"
import { loginUser } from "../api/UserAPI"
import { useUser } from "../Context/UserContext"
import { storageSave } from "../utils/storage"
import { useNavigate } from "react-router-dom"

const usernameConfig = {
    required: true,
    minLength: 3
}


const LoginForm = ()=> {

const {register, handleSubmit, formState: { errors } } = useForm()
const {user, setUser} = useUser()
const navigate = useNavigate()

useEffect(()=> {
    if(user !== null) {
        navigate('Translation')
    }

}, [user, navigate])

const onSubmit = async (data) =>{

    const [error, userObj]  = await loginUser(data.username);
    console.log(error);
    if (error === null) {
        setUser(userObj);
        storageSave('translationUser', userObj)
    }
    console.log(data);
}


    return(
        <>
            <h1>Log in</h1>
            <form onSubmit={handleSubmit(onSubmit)}>
                <fieldset>
                <label htmlFor="username"> Username: </label>
                <input type="text" placeholder="johndoe" 
                {...register("username", usernameConfig)} />
                {(errors.username && errors.username.type === 'required') && <span>Username is required</span>}
                {(errors.username && errors.username.type === 'minLength') && <span>Min length 3</span>}
                </fieldset>
                <button type="submit"> Log in </button>
            </form>
        </>
    )
}




export default LoginForm