import { useUser } from "../Context/UserContext"
import { storageSave } from "../utils/storage"

const LogOutButton = () =>{

    const {setUser} = useUser(); //get setuser from the context API
    const handleClick = () => {
        setUser(null); // set user to null in contextAPI
        storageSave('translationUser', null); //set user in storage to null
        //no need for redirects as it is handled by the withAuth hoc.
    }


    return(
        <div>
            <button onClick={handleClick}>Log Out</button>
        </div>
    )
}

export default LogOutButton