import { updateIndex } from "../api/UserAPI";
import { useUser } from "../Context/UserContext";
import { storageSave } from "../utils/storage"

const DeleteButton = () => {
  const { user, setUser } = useUser(); //get user and setuser from userhook contextAPI


  const handleClick = async () => {
    let translationLength = user.translations.length; //get the length of the current translation array
    const [error, updatedUser] = await updateIndex(user, translationLength); //set index on the user in userAPI to the length of array
    // When the index is set, the TranslationList component will only render translations with index higher than the index
    // By setting the user index to the length of the array, the translations made prior to this click, will no longer be rendered
    // but they are still stored on the APIs server, and are still retrieved, just not shown to the user
    if (error === null) {
        //update user object in contextAPI to reflect changes made here
      setUser(updatedUser);
      storageSave('translationUser', updatedUser)
    } else {
        console.error(error);
    }
  };

  return (
        <>
        <button onClick={handleClick}>Delete Translation History</button>
        </>
  );
  
};

export default DeleteButton;
