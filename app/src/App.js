import './App.css';
import Login from './View/Login'
import {BrowserRouter, Routes, Route} from 'react-router-dom'
import Translation from './View/Translation';
import Profile from './View/Profile';

function App() {
  return (

    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Login/>}/>
          <Route path="/translation" element={<Translation/>}/>
          <Route path="/profile" element={<Profile/>}/>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
