const apiKey = process.env.REACT_APP_API_KEY;
const apiURL = process.env.REACT_APP_API_URL;

/**
 * Gets a user from the translations userAPI
 * Returns an array of length 2 where the first is an error string
 * and second item is a user
 * If error is null, and user is not null, retrieval of user was successfull
 * @param String
 *  @returns {Array<String,obj>}
 */
export const getUser = async (username) => {
  try {
    const response = await fetch(`${apiURL}translations?username=${username}`);
    const data = await response.json();
    if (data.length > 0) {
      return [null, data[0]];
    }
    return ["Error: could not find user", null];
  } catch (error) {
    console.error(error);
    return [error.message, null];
  }
};

/**
 * Registers a user to the translations userAPI
 * Returns an array of length 2 where the first is an error string
 * and second item is the registered user object, containing the users id
 * If error is null, and user is not null, registering the user was successfull
 * @param String
 *  @returns {Array<String,obj>}
 */
const registerUser = async (username) => {
  try {
    const response = await fetch(`${apiURL}translations`, {
      method: "POST",
      headers: {
        "X-API-Key": apiKey,
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        username: `${username}`,
        translations: [],
      }),
    });
    if (!response.ok) {
      return [
        "Error: could not create new user, response from api is not ok",
        null,
      ];
    }
    const user = await response.json();
    return [null, user];
  } catch (error) {
    console.error(error);
    return [error.message, null];
  }
};

/**
 * Logins a user, by either retrieving their user data from the database
 * or registering them and returning their new userdata
 * Returns an array of length 2 where the first is an error string
 * and second item is the user object
 * If error is null, and user is not null, login of the user was successfull
 * @param String
 *  @returns {Array<String,obj>}
 */
export const loginUser = async (username) => {

  // check if the username is empty or just spaces
  if (!username || username.trim().length === 0) {
    return ["Error: username is empty", null];
  }
  const checkForUser = await getUser(username);
  //checks if the user already exists and if it does returns it
  if (checkForUser[0] === null) {
    return [null, checkForUser[1]];
  }
  const registeredUser = await registerUser(username);
  if (registeredUser[0] === null) {
    //checks if the register was successfull, if it was return the new user
    return [null, registeredUser[1]];
  }
  return [
    "Error: Could not find any user matching that username, and could not make a new user",
    null,
  ];
};

/**
 * Saves a translation to the user in the userAPI
 * Returns an array of length 2 where the first is an error string
 * and second item is the updated user object, containing all translations
 * If error is null, and user is not null, updating users translations was successfull
 * @param object
 * @param String
 *  @returns {Array<String,obj>}
 */
export const updateTranslations = async (user, newTranslation) => {
  const { id, translations } = user;
  try {
    const response = await fetch(`${apiURL}translations/${id}`, {
      method: "PATCH",
      headers: {
        "X-API-Key": apiKey,
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        translations: [...translations, newTranslation], //append new translation to the old ones
      }),
    });
    if (!response.ok) {
      console.error("Error: Could not update the translations of user");
      return ["Error: Could not update the translations of user", null];
    }
    const user = await response.json();
    return [null, user];
  } catch (error) {
    console.error(error);
    return [error.message, null];
  }
};


/**
 * Saves the index to the user in the userAPI
 * Returns an array of length 2 where the first is an error string
 * and second item is the updated user object, containing the updated/created index
 * If error is null, and user is not null, updating the users index was successfull
 * @param object
 * @param num
 *  @returns {Array<String,obj>}
 */
export const updateIndex = async (user, index) => {
  const { id } = user;
  try {
    const response = await fetch(`${apiURL}translations/${id}`, {
      method: "PATCH",
      headers: {
        "X-API-Key": apiKey,
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        index: index, //update/create index property
      }),
    });
    if (!response.ok) {
      console.error("Error: Could not update the index of user");
      return ["Error: Could not update the index of user", null];
    }
    const user = await response.json();
    return [null, user];
  } catch (error) {
    console.error(error);
    return [error.message, null];
  }
};
