# Lost in Translation App
heroku-url: https://enigmatic-basin-29677.herokuapp.com/

## Contributors:
Silja Stubhaug Torkildsen - gitlab.com/SiljaTorki 

Jonas Svåsand - gitlab.com/jsva

## Description:
An app that lets you translate plain text to images representing signs in American Sign Language (ASL).
The translation will be shown to you, so that you can replicate it if you wish to use ASL.
The app only translate space and letters, special characters are ignored and will not show.
The app requires a username, and will store your translations for you. You can view the latest translations by going to your profile.
You can clear the translation history from being shown in the profile page, but note that it will not be deleted from the database, all your translations
will be stored permanently. 
Your username will be stored between sessions, so you won't be asked again for it, if you have provided it previously. If you wish to log out of the application you can do so from the profile page.



## Instructions

**You need to have npm installed**

1. clone this repo
2. navigate to the folder you cloned the project to and open a command prompt
3. navigate to app - `cd app`
4. `npm install`
5. `npm start`
6. Go to the the adress, provided in the command prompt, in your browser.
7. You can now use the app.
8. Enter a user (can not be empty). Usernames are case sensitive, and unique 
9. Enter a translation into the textbox and submit the translation using the button
10. If you wish to see your translation history, or log out, navigate to the profile page using the navigation bar at the top of the app.

Alternatively you can use the app by visiting the website url provided at the top.


## Project status

- User can log in and will be stored in the API. If the user allredy are logged in the user will be redirected to the Translation page.
- The text that he user types in the input box will be tranlated and shown with images on the page under the form. 
- In the component tree it is a "Show translationresult", that component has not been used, instead the result is show in the "TranslationForm" component.
- The Profile page displays the 10 last translations for the current user, the user can clear the transitions with a "delete"-button and log out.
- The component tree shows a "Translation Item" component, in the app only "Translation list" is used and Translation Items is created there.


